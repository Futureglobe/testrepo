"use strict";
var editorController = new EditorController();
function EditorController()
{
    this.codeEditor = null;
    
    // Setting up the code editor and initializing the ace editor
    this.setupEditor = function()
    {
        editorController.codeEditor = ace.edit("codeEditor"); 
        editorController.codeEditor.$blockScrolling = Infinity;
        editorController.applyEditorSettings();   
    }

    // Changing the code editor theme
    this.changeTheme = function(name){editorController.codeEditor.setTheme("ace/theme/" + name);}

    // Changing the language mode fo the code editor
    this.changeMode = function(name){editorController.codeEditor.getSession().setMode("ace/mode/" + name);}

    // Updating the code editor to remain the font and general sizes
    this.updateEditor = function()
    {
        var pref = preferenceManager.getPreference("editorPreference");
        editorController.codeEditor.resize();
        editorController.codeEditor.setFontSize(0);
        editorController.codeEditor.setFontSize(pref.fontSize);
    }
    
    // Changing the font size of the code editor
    this.changeFontSize = function(size)
    {
        editorController.codeEditor.setFontSize(0);
        editorController.codeEditor.setFontSize(size);
    }

    // Applying the code editor settings based on the saved values in the editorPreferences
    this.applyEditorSettings = function()
    {
        var pref = preferenceManager.getPreference("editorPreference");
        this.codeEditor.setOptions({
            highlightActiveLine: pref.highlightActiveLine,
            fontSize: pref.fontSize,
            readOnly: pref.readOnly,
            hScrollBarAlwaysVisible: false,
            vScrollBarAlwaysVisible: false,
            showLineNumbers: pref.showLineNumbers,
            showInvisibles: pref.showInvisibles,
            showPrintMargin: false,
            fadeFoldWidgets: pref.fadeFoldWidgets,
            showFoldWidgets: pref.showFoldWidgets,
            displayIndentGuides: pref.displayIndentGuides,
            scrollSpeed: pref.scrollSpeed,
            dragEnabled: pref.dragEnabled,
            useSoftTabs: pref.useSoftTabs,
            tabSize: pref.tabSize,
            wrap: pref.wrap,
            enableMultiselect: pref.enableMultiselect,
            enableBasicAutocompletion: pref.enableBasicAutocompletion,
            enableSnippets: pref.enableSnippets,
            enableLiveAutocompletion: pref.enableLiveAutocompletion,
            theme: "ace/theme/" + pref.theme
        });
    }
}