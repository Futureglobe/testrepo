"use strict";
const { app, Menu, dialog, BrowserWindow, globalShortcut, Tray, shell} = require("electron");
const settings = require('electron-settings');
const path = require("path");
const url = require("url");
var ipc = require("electron").ipcMain;
const windowStateKeeper = require("electron-window-state");
var mainWindow = null;
var tray = null;
if (require("electron-squirrel-startup")) return;
var menuText = {};
var appMenu = {};wadwadwad
        defaultWidth: 1280,
        defaultHeight: 860
    });

    // Tray icon click event 
    tray.on("click", function () 
    {
        mainWindow.webContents.send("closeQuickSearch");
        mainWindow.webContents.send("closeQuickCreate");
        mainWindow.webContents.send("showMainView");
        mainWindow.show();
    });

    try 
    {
        // Registering global shortcuts
        globalShortcut.register("CommandOrControl+Alt+S", () => 
        {
            mainWindow.webContents.send("openQuickSearch");
            mainWindow.show();
            Menu.setApplicationMenu(quickSearchMenu);
        });

        globalShortcut.register("CommandOrControl+Alt+N", () => 
        {wada
        });

        globalShortcut.register("CommandOrControl+Alt+O", () => 
        {
            mainWindow.webContents.send("closeQuickSearch");
            mainWindow.webContents.send("closeQuickCreate");
            mainWindow.webContents.send("showMainView");
            mainWindow.show();
            Menu.setApplicationMenu(appMenu);
        });

        var useGlobalShortcuts = settings.get("generalPreference").useGlobalShortcuts;
        if(useGlobalShortcuts == "0")
        {
            globalShortcut.unregisterAll();
        }
    } 
    catch (error) {}

    // Main window 
    mainWindow = new BrowserWindow
    ({
        minWidth: 1000,
        minHeight: 700,
        "x": mainWindowState.x,
        "y": mainWindowState.y,
        "width": mainWindowState.width,
        "height": mainWindowState.height,
        frame: true,
        center: true,
        backgroundColor: "#f4f4f4",
        icon: path.join(__dirname, "app/ressources/images/appIcon.png"),
    });
    mainWindowState.manage(mainWindow);

    // Loading main index file to use for main window 
    mainWindow.loadURL(url.format
    ({
        pathname: path.join(__dirname, "./app/views/main/index.html"),
        protocol: "file:",
        slashes: true
    }));
    // mainWindow.show();
    if(releaseBuild == false) 
    {
        mainWindow.openDevTools();
    }

    // Catching the close event of the main window 
    mainWindow.on("close", function(event) 
    {
        // Preventing the default behaviour if the app is just closed but not exited and only hiding the main window 
        if (!app.isQuiting) 
        {
            event.preventDefault();
            mainWindow.hide();
        }
        return false;
    });
});

// Listening on ipc events send from the renderer process 
// Sending the current app path to the renderer process 
ipc.on("sendAppPath", function(event) 
{
    mainWindow.webContents.send("getAppPath", app.getAppPath());
});

ipc.on("sendUserDocumentsPath", function(event) 
{
    mainWindow.webContents.send("getUserDocumentPath", app.getPath("documents"));
});

ipc.on("sendUserDataPath", function(event) 
{
    mainWindow.webContents.send("getUserDataPath", app.getPath("userData"));
});

// Setting the app menu to the main menu template when the quickSearch Overlay is closed 
ipc.on("quickSearchClosed", function(event) 
{
    Menu.setApplicationMenu(appMenu);
});

ipc.on("quickCreateClosed", function(event) 
{
    Menu.setApplicationMenu(appMenu);
});

ipc.on("openQuickSearch", function(event) 
{
    Menu.setApplicationMenu(quickSearchMenu);
});

ipc.on("openQuickCreate", function(event) 
{
    Menu.setApplicationMenu(quickCreateMenu);
});

ipc.on("openDeveloperConsole", function()
{
    mainWindow.openDevTools();
});

ipc.on("setStoragePath", function() 
{
    dialog.showOpenDialog(mainWindow,
    {
        properties: ["openDirectory"]
    }, 
    function(paths) 
    {
        mainWindow.webContents.send("sendStoragePath", paths);
    });
})

ipc.on("openStorageFolderInExplorer", function(event, storagePath) 
{
    shell.showItemInFolder(storagePath);
});

ipc.on("updateMenuLang", function(event, passedLanguageData) 
{
    menuText = passedLanguageData;
    var appMenuTemplate =
    [
        {
            label: menuText.app_label,
            submenu:
            [
                {
                    label: menuText.settings_label,
                    accelerator: "CmdOrCtrl+P",
                    click() {mainWindow.webContents.send("openSettings")}
                },
                {
                    label: menuText.importandexport_label,
                    submenu:
                    [
                        {
                            label: menuText.importsettingsfromzip_label,
                            click() 
                            {
                                dialog.showOpenDialog(mainWindow,
                                {
                                    title: menuText.importsettingsfromzip_dialog_title,
                                    filters:
                                    [
                                        {name: menuText.importfromzip_dialog_ziparchive_extension_label, extensions: ["zip"]}
                                    ],
                                    buttonLabel: menuText.importfromzip_dialog_action_label
                                },
                                function(filePath) 
                                {
                                    if(filePath != null) 
                                    {
                                        mainWindow.webContents.send("importSettingsFromZIPFile", filePath);
                                    }
                                });
                            }
                        },
                        {
                            label: menuText.exportsettingsfromzip_label,
                            click() 
                            {
                                dialog.showSaveDialog(mainWindow,
                                {
                                    title: menuText.exportsettingsfromzip_dialog_title,
                                    filters:
                                    [
                                        {name: menuText.importfromzip_dialog_ziparchive_extension_label, extensions: ["zip"]}
                                    ],
                                    buttonLabel: menuText.exporttozip_dialog_action_label
                                },
                                function(filePath) 
                                {
                                    if(filePath != null) 
                                    {
                                        mainWindow.webContents.send("exportSettingsFromZIPFile", filePath);
                                    }
                                });
                            }
                        },
                        {
                            label: menuText.importfromzip_label,
                            click()     
                            {
                                dialog.showOpenDialog(mainWindow,
                                {
                                    title: menuText.importfromzip_dialog_title,
                                    filters:
                                    [
                                        {name: menuText.importfromzip_dialog_ziparchive_extension_label, extensions: ["zip"]}
                                    ],
                                    buttonLabel: menuText.importfromzip_dialog_action_label
                                },
                                function(filePath) 
                                {
                                    if(filePath != null) 
                                    {
                                        mainWindow.webContents.send("importFromZIPFile", filePath);
                                    }
                                });
                            }
                        },
                        {
                            label: menuText.exportaszip_label,
                            click() 
                            {
                                dialog.showSaveDialog(mainWindow,
                                {
                                    title: menuText.exporttozip_dialog_title,
                                    filters:
                                    [
                                        {name: menuText.importfromzip_dialog_ziparchive_extension_label, extensions: ["zip"]}
                                    ],
                                    buttonLabel: menuText.exporttozip_dialog_action_label
                                },
                                function(filePath) 
                                {
                                    if(filePath != null) 
                                    {
                                        mainWindow.webContents.send("exportAsZIPFile", filePath);
                                    }
                                });
                            }
                        }
                    ]
                },
                {
                    label: menuText.restart_app_label,
                    accelerator: "CmdOrCtrl+R",
                    click() 
                    {
                        if(releaseBuild === false) 
                        {
                            mainWindow.reload();
                        }
                        else 
                        {
                            var choice = dialog.showMessageBox(mainWindow,
                            {
                                type: "info",
                                buttons: [menuText.yes_label, menuText.no_label],
                                title: menuText.restart_app_question_label,
                                center: true,
                                message: menuText.restart_app_question_text
                            });
                            if(choice == 0) 
                            {
                                app.relaunch();
                                app.exit(0);
                            }
                        }
                    }
                },
                {
                    label: menuText.checkforupdate_label,
                    click() { mainWindow.webContents.send("checkForUpdates"); }
                },
                {
                    label: menuText.viewchangelog_label,
                    click() 
                    {
                        const { shell } = require("electron");
                        shell.openExternal("http://buildserver.futureglobe.de/SnipAway/Changelogs/index.html");
                    }
                },
                {
                    label: menuText.exit_label,
                    click() 
                    {
                        var choice = dialog.showMessageBox(mainWindow,
                        {
                            type: "info",
                            buttons: [menuText.yes_label, menuText.no_label],
                            title: menuText.exit_app_label,
                            center: true,
                            message: menuText.exit_app_question_text,
                            cancelId: 1
                        });
                        if (choice == 0) 
                        {
                            app.exit();
                        }
                    }
                }
            ]
        },
        {
            label: menuText.file_label,
            submenu:
            [
                {
                    label: menuText.create_snippet_label,
                    accelerator: "CmdOrCtrl+N",
                    click() { mainWindow.webContents.send("createNewSnippet"); }
                },
                {
                    label: menuText.copy_snippet_code_label,
                    accelerator: "CmdOrCtrl+Shift+c",
                    click() { mainWindow.webContents.send("copySnippetCode"); }
                },
                {
                    label: menuText.delete_snippet_label,
                    accelerator: "CmdOrCtrl+Shift+d",
                    click() { mainWindow.webContents.send("deleteSnippetCode"); }
                },
                {
                    label: menuText.mark_snippet_as_favorite,
                    accelerator: "CmdOrCtrl+Shift+f",
                    click() { mainWindow.webContents.send("markSnippetAsFavorite"); }
                }
            ]
        },
        {
            label: menuText.view_label,
            submenu:
            [
                {
                    label: menuText.toggle_fullscreen_label,
                    accelerator: "Alt+F",
                    click() 
                    {
                        var isFullScreen = mainWindow.isFullScreen();
                        switch (isFullScreen) 
                        {
                            case true:
                                mainWindow.setFullScreen(false);
                                break;
                            case false:
                                mainWindow.setFullScreen(true);
                                break;
                        }
                    }
                },
                {
                    label: menuText.reload_snippet_label,
                    accelerator: "Alt+R",
                    click() { mainWindow.webContents.send("reloadSnippets"); }
                },
                {
                    label: menuText.focus_snippet_label,
                    accelerator: "Alt+E",
                    click() { mainWindow.webContents.send("focusSnippet"); }
                },
                {
                    label: menuText.focus_search_label,
                    accelerator: "Alt+S",
                    click() { mainWindow.webContents.send("focusSearchField"); }
                },
                {
                    label: menuText.toggle_snippet_area_label,
                    accelerator: "CmdOrCtrl+Alt+D",
                    click() {
                        mainWindow.webContents.send("toggleMainPaneFullscreen");
                    }
                },
                {
                    label: menuText.toggle_editor_fullscreen_label,
                    accelerator: "CmdOrCtrl+Alt+F",
                    click() { mainWindow.webContents.send("toggleEditorFullscreen"); }
                }
            ]
        },
        {
            label: menuText.help_label,
            submenu:
            [
                {
                    label: menuText.report_bug_label,
                    click()
                    {
                        const { shell } = require("electron");
                        shell.openExternal("https://github.com/Futureglobe/SnipAway/issues/new?template=bug_report.md");
                    }
                },
                {
                    label: menuText.request_feature_label,
                    click()
                    {
                        const { shell } = require("electron");
                        shell.openExternal("https://github.com/Futureglobe/SnipAway/issues/new?template=feature_request.md");
                    }
                },
                {
                    label: menuText.contactUs_label,
                    click()
                    {
                        dialog.showMessageBox(mainWindow,{
                            message: menuText.contactUs_text
                        });
                    }
                }
            ]
        }
    ]

    // Quicksearch app menu template 
    var quickSearchMenuTemplate =
    [
        {
            label: menuText.app_label,
            submenu:
            [
                {
                    label: menuText.exit_label,
                    click() 
                    {
                        var choice = dialog.showMessageBox(mainWindow,
                        {
                            type: "info",
                            buttons: [menuText.yes_label, menuText.no_label],
                            title: menuText.exit_app_label,
                            center: true,
                            cancelId: 1,
                            message: menuText.exit_app_question_text
                        });
                        if (choice == 0) 
                        {
                            app.exit();
                        }
                    }
                }
            ]
        },
        {
            label: menuText.file_label,
            submenu:
            [
                {
                    label: menuText.copy_snippet_code_label,
                    accelerator: "CmdOrCtrl+Shift+c",
                    click() { mainWindow.webContents.send("quickSearchCopySnippetCode"); }
                }
            ]
        },
        {
            label: menuText.view_label,
            submenu:
            [
                {
                    label: menuText.focus_search_label,
                    accelerator: "Alt+S",
                    click() { mainWindow.webContents.send("quickSearchFocusSearchField"); }
                }
            ]
        }
    ]

    // Quicksearch app menu template 
    var quickCreateMenuTemplate =
    [
        {
            label: menuText.app_label,
            submenu:
            [
                {
                    label: menuText.exit_label,
                    click() 
                    {
                        var choice = dialog.showMessageBox(mainWindow,
                        {
                            type: "info",
                            buttons: [menuText.yes_label, menuText.no_label],
                            title: menuText.exit_app_label,
                            center: true,
                            cancelId: 1,
                            message: menuText.exit_app_question_text
                        });
                        if(choice == 0) 
                        {
                            app.exit();
                        }
                    }
                }
            ]
        }
    ]

    // Bulding/Instantiating tray context menu 
    trayContextMenu = Menu.buildFromTemplate(
    [
        {
            label: menuText.show_app_label, click() 
            {
                mainWindow.show();
                mainWindow.webContents.send("closeQuickSearch");
                mainWindow.webContents.send("closeQuickCreate");
                mainWindow.webContents.send("showMainView");
            },
            accelerator: "Alt+CmdOrCtrl+O"
        },
        { label: menuText.quick_create_label, click() {mainWindow.show(); mainWindow.webContents.send("openQuickCreate"); Menu.setApplicationMenu(quickCreateMenu); }, accelerator: "CmdOrCtrl+Alt+N" },
        { label: menuText.quick_search_label, click() {mainWindow.show(); mainWindow.webContents.send("openQuickSearch"); Menu.setApplicationMenu(quickSearchMenu); }, accelerator: "CmdOrCtrl+Alt+S" },
        {
            label: menuText.exit_label, click() 
            {
                var choice = dialog.showMessageBox(mainWindow,
                {
                    type: "info",
                    buttons: [menuText.yes_label, menuText.no_label],
                    title: menuText.exit_app_label,
                    center: true,
                    cancelId: 1,
                    message: menuText.exit_app_question_text
                });
                if (choice == 0)
                {
                    app.exit();
                }
            }
        },
    ]);

    appMenu = Menu.buildFromTemplate(appMenuTemplate);
    quickSearchMenu = Menu.buildFromTemplate(quickSearchMenuTemplate);
    quickCreateMenu = Menu.buildFromTemplate(quickCreateMenuTemplate);
    Menu.setApplicationMenu(appMenu);
    tray.setContextMenu(trayContextMenu);
    // Text to show when hovering over tray icon 
    tray.setToolTip(menuText.app_label);
});

